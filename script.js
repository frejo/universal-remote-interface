/* Definer profiler */
var profile = 1;
var isDown = { number: 0, state: false };
var xhttp = new XMLHttpRequest();

function changeProfile( newProfile ) {
    /* Fjern klassen fra den profilknap, der er aktiv, og tilføj klassen til den nye aktive profil */
    el = document.getElementById( 'P' + profile );
    newEl = document.getElementById( 'P' + newProfile );
    if ( hasClass( el, 'active' ) ) {
        removeClass( el, 'active' );
    }
    addClass( newEl, 'active' );
    profile = newProfile;
};

function sendPost( button, state ) {
    if ( state == true ) {
        state = "ON";
    } else {
        state = "OFF";
    }
    /* Definer payload */
    var payload = "profile=" + profile + "&button=" + button + "&state=" + state;
    console.log( "POSTing payload " + payload );
    /* Lav og send POST-request til ESP på "/post". */
    xhttp.open( "POST", "/post", false ); /* false siger at vi ikke sender den async, dvs. at vi i stedet venter på et svar før scriptet fortsætter */
    xhttp.setRequestHeader( 
        "Content-Type",
        "application/x-www-form-urlencoded; charset=UTF-8" );
    xhttp.send( payload );
};

function buttonDown( button ) {
    /* Når en knap trykkes sættes isDown til hvilken knap, og true til at den er trykket ned */
    console.log( "Button " + button + " is pressed." );
    isDown.number = button;
    isDown.state = true;
    loopSend();
};

function buttonUp( button ) {
    console.log( "Button " + button + " is released." );
    isDown.number = button;
    isDown.state = false;
};

function loopSend() {
    /* Der skal blive ved med at sende en POST-request indtil buttonUp har sat isDown.state til false, med mindre der er trykket på knap 3, der ændrer profil */
    if ( isDown.state == true ) {
        sendPost( isDown.number, isDown.state )
        if ( isDown.number == 3 ) {
            /* Når knap 3 trykkes, skiftes profil til næste. Det går i ring 1-2-3-4-1-2-3-4... */
            if ( profile == 4 ) {
                changeProfile( 1 );
            } else {
                changeProfile( profile + 1 );
            }
            isDown.state = false;
        }
        setTimeout( loopSend, 50 ); /* Vent 50 ms (  denne funktion fryser ikke siden  ) og kør sendLoop igen efter */
    }
}

// For at kunne tilføje specifikt design til profilknappen, der er valgt, skal der kunne tilføjes og fjernes html-class. Disse funktioner er hentet fra https://jaketrent.com/post/addremove-classes-raw-javascript/:
function hasClass( el, className ) {
    if ( el.classList )
        return el.classList.contains( className )
    else
        return !!el.className.match( new RegExp( '(\\s|^)' + className + '(\\s|$)' ) )
}

function addClass( el, className ) {
    if ( el.classList )
        el.classList.add( className )
    else if ( !hasClass( el, className ) ) el.className += " " + className
}

function removeClass( el, className ) {
    if ( el.classList )
        el.classList.remove( className )
    else if ( hasClass( el, className ) ) {
        var reg = new RegExp( '(\\s|^)' + className + '(\\s|$)' )
        el.className = el.className.replace( reg, ' ' )
    }
}


//Kør changeProfile når scriptet loades for at give standard-profilen den aktive html-class
changeProfile( profile );